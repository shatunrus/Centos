#!/bin/sh
HWADDR=$(cat /sys/class/net/e*/address)
ips=$(ip a | grep -v 127 | grep -E "\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b" | awk '{ print $2" "$NF"\\n" }')

dialog --title "Sysinfo" --msgbox "\nMAC: $HWADDR\nip:\n$(echo $ips |  sed 's/^ //g') " 10 100
clear

